import { https } from './api';

export default class LoginService {
  constructor() {
    this.instance = https('/login');
  }

  login = ( data ) => {
    return this.instance({
      method: 'POST',
      data
    });
  }

}