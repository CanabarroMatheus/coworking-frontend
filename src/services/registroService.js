import { https } from './api';

export default class RegistroService {
  constructor() {
    this.instance = https('/api/usuario');
  }

  registrar = ( data ) => {
    return this.instance({
      method: 'POST',
      url: '/salvar',
      data
    })
  }
}