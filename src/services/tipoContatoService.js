import { obterLoginDataDoStorage } from '../helpers/storage';
import { https } from './api';

export default class TipoContatoService {
  constructor() {
    this.loginToken = obterLoginDataDoStorage();
    this.instance = https( '/api/tipoContato' );
  }

  trazerTodosTipoContato = () => {
    return this.instance({
      headers: {
        Authorization: this.loginToken
      },
      method: 'GET',
      url: '/trazer/todos'
    });
  }

  trazerPorId = ( id ) => {
    return this.instance({
      headers: {
        Authorization: this.loginToken
      },
      method: 'GET',
      url: `/trazer/id/${ id }`
    });
  }

  trazerPorNome = ( nome ) => {
    return this.instance({
      headers: {
        Authorization: this.loginToken
      },
      method: 'GET',
      url: `/trazer/nome/${ nome }`
    });
  }

  novoTipoContato = ( data ) => {
    return this.instance({
      headers: {
        Authorization: this.loginToken
      },
      method: 'POST',
      url: `/salvar`,
      data
    });
  }

  atualizarContato = ( id, data ) => {
    return this.instance({
      headers: {
        Authorization: this.loginToken
      },
      method: 'PUT',
      url: `/editar/${ id }`,
      data
    });
  }
}