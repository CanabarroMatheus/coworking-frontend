import { obterLoginDataDoStorage } from '../helpers/storage';
import { https } from './api';

export default class PacoteService {
  constructor() {
    this.loginToken = obterLoginDataDoStorage();
    this.instance = https( '/api/pacote' )
  }

  trazerTodosPacotes = () => {
    return this.instance({
      headers: {
        Authorization: this.loginToken
      },
      method: 'GET',
      url: '/trazer/todos'
    });
  }

  trazerPacotePorId = ( id ) => {
    return this.instance({
      headers: {
        Authorization: this.loginToken
      },
      method: 'GET',
      url: `/trazer/id/${ id }`
    });
  }

  novoPacote = ( data ) => {
    return this.instance({
      headers: {
        Authorization: this.loginToken
      },
      method: 'POST',
      url: '/salvar',
      data
    });
  }

  atualizarPacote = ( id, data ) => {
    return this.instance({
      headers: {
        Authorization: this.loginToken
      },
      method: 'PUT',
      url: `/editar/${ id }`,
      data
    });
  }
}