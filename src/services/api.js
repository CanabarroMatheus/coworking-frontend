import axios from 'axios';

export const https = ( baseUrl = '' ) => {
  const instance = axios.create({
    baseURL: `http://localhost:8080${ baseUrl }`
  });
  return instance;
}