import { obterLoginDataDoStorage } from '../helpers/storage';
import { https } from './api';

export default class PagamentoService {
  constructor() {
    this.loginToken = obterLoginDataDoStorage();
    this.instance = https('/api/pagamento');
  }

  trazerTodosPagamentos = () => {
    return this.instance({
      headers: {
        Authorization: this.loginToken
      },
      method: 'GET',
      url: '/trazer/todos'
    });
  }

  encontrarPagamentoPorId = ( id ) => {
    return this.instance({
      headers: {
        Authorization: this.loginToken
      },
      method: 'GET',
      url: `/trazer/id/${ id }`
    });
  }

  salvarPagamento = ( data ) => {
    return this.instance({
      headers: {
        Authorization: this.loginToken
      },
      method: 'POST',
      url: '/salvar',
      data
    });
  }
}