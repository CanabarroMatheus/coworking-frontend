import { obterLoginDataDoStorage } from '../helpers/storage';
import { https } from './api';

export default class ClienteService {
  constructor() {
    this.loginToken = obterLoginDataDoStorage();
    this.instance = https('/api/cliente');
  }

  trazerTodosClientes = () => {
    return this.instance({
      headers: {
        Authorization: this.loginToken
      },
      method: 'GET',
      url: '/trazer/todos'
    });
  }

  trazerClientePorId = ( id ) => {
    return this.instance({
      headers: {
        Authorization: this.loginToken
      },
      method: 'GET',
      url: `/trazer/id/${ id }`
    });
  }

  novoCliente = ( data ) => {
    return this.instance({
      headers: {
        Authorization: this.loginToken
      },
      method: 'POST',
      url: '/salvar',
      data
    });
  }

  atualizarCliente = ( id, data ) => {
    return this.instance({
      headers: {
        Authorization: this.loginToken
      },
      method: 'PUT',
      url: `/editar/${ id }`,
      data
    });
  }
}