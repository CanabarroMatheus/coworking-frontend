import { obterLoginDataDoStorage } from '../helpers/storage';
import { https } from './api';

export default class EspacoService {
  constructor() {
    this.loginToken = obterLoginDataDoStorage();
    this.instance = https( '/api/espaco' );
  }

  trazerTodosEspacos = () => {
    return this.instance({
      headers: {
        Authorization: this.loginToken
      },
      method: 'GET',
      url: '/trazer/todos'
    });
  }

  trazerEspacoPorId = ( id ) => {
    return this.instance({
      headers: {
        Authorization: this.loginToken
      },
      method: 'GET',
      url: `/trazer/id/${ id }`
    });
  }

  novoEspaco = ( data ) => {
    return this.instance({
      headers: {
        Authorization: this.loginToken
      },
      method: 'POST',
      url: '/salvar',
      data
    });
  }

  atualizarEspaco = ( id, data ) => {
    return this.instance({
      headers: {
        Authorization: this.loginToken
      },
      method: 'PUT',
      url: `/editar/${ id }`,
      data
    });
  }
}