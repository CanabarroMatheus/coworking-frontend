import { obterLoginDataDoStorage } from '../helpers/storage';
import { https } from './api';

export default class ClientePacoteService {
  constructor() {
    this.loginToken = obterLoginDataDoStorage();
    this.instance = https('/api/clientePacote');
  }

  trazerTodosClientePacotes = () => {
    return this.instance({
      headers: {
        Authorization: this.loginToken
      },
      method: 'GET',
      url: '/trazer/todos'
    });
  }

  trazerPorId = ( id ) => {
    return this.instance({
      headers: {
        Authorization: this.loginToken
      },
      method: 'GET',
      url: `/trazer/id/${ id }`
    });
  }

  salvarClientePacote = ( data ) => {
    return this.instance({
      headers: {
        Authorization: this.loginToken
      },
      method: 'POST',
      url: '/salvar',
      data
    });
  }
}