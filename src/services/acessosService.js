import { obterLoginDataDoStorage } from '../helpers/storage';
import { https } from './api';

export default class AcessosService {
  constructor() {
    this.loginToken = obterLoginDataDoStorage();
    this.instance = https('/api/acesso')
  }

  entrar = ( data ) => {
    return this.instance({
      headers: {
        Authorization: this.loginToken
      },
      method: 'POST',
      url: '/entrar',
      data
    });
  }

  entrarEspecial = ( data ) => {
    return this.instance({
      headers: {
        Authorization: this.loginToken
      },
      method: 'POST',
      url: '/entrar/especial',
      data
    });
  }

  sair = ( data ) => {
    return this.instance({
      headers: {
        Authorization: this.loginToken
      },
      method: 'POST',
      url: '/sair',
      data
    });
  }
}