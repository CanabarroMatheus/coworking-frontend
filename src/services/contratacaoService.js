import { obterLoginDataDoStorage } from '../helpers/storage';
import { https } from './api';

export default class ContratacaoService {
  constructor() {
    this.loginToken = obterLoginDataDoStorage();
    this.instance = https('/api/contratacao');
  }

  trazerTodasContratacoes = () => {
    return this.instance({
      headers: {
        Authorization: this.loginToken
      },
      method: 'GET',
      url: '/trazer/todos'
    });
  }

  trazerContratacaoPorId = ( id ) => {
    return this.instance({
      headers: {
        Authorization: this.loginToken
      },
      method: 'GET',
      url: `/trazer/id/${ id }`
    });
  }

  novaContratacao = ( data ) => {
    return this.instance({
      headers: {
        Authorization: this.loginToken
      },
      method: 'POST',
      url: '/salvar',
      data
    });
  }
}