import { useEffect, useState } from 'react';
import Box from '../../components/box/Box';
import AntButton from '../../components/middleware/AntButton';
import { AntModal } from '../../components/middleware/AntModal';
import AntTable from '../../components/middleware/AntTable';
import EspacoForm from '../../components/middleware/Form/EspacoForm';
import { openErrorNotification, openSuccessNotification } from '../../components/middleware/Notifications';
import { tabelaEspaco } from '../../constants/tabelas';
import EspacoService from '../../services/espacoService'
import './listar-espaco.css';

function ListarEspaco() {
  const [espacos, setEspacos] = useState( false );
  const [mostrarModal, setMostrarModal] = useState( false );

  const cadastrarEspaco = ( data ) => {
    const service = new EspacoService();
    service.novoEspaco( data )
      .then( () => {
        openSuccessNotification({
          mensagem: 'Sucesso!',
          descricao: 'Espaço cadastrado com sucesso!'
        });
      } )
      .catch(() => {
        openErrorNotification({
          mensagem: 'Erro',
          descricao: 'Houve um erro ao tentar cadastrar o espaço, tente novamente mais tarde...'
        });
      })
  }

  useEffect(() => {
    const service = new EspacoService();
    service.trazerTodosEspacos()
      .then( ({ data }) => {
        setEspacos( data );
      } );
  },[espacos])

  return (
    <div className="listar-espaco">
      <Box>
        <AntButton tipo="primary" clique={ () => setMostrarModal( true ) }>Cadastrar</AntButton>
      </Box>
      <Box>
        <AntTable data={ espacos } colunas={ tabelaEspaco } />
      </Box>
      <AntModal titulo="Cadastrar novo espaço" visivel={ mostrarModal } cancelar={ () => setMostrarModal( false ) } cancelarTexto="" okTexto="" botaoOkProps={ { hidden: true } } botaoCancelarProps={ { hidden: true } } >
        <EspacoForm acao={ cadastrarEspaco } />
      </AntModal>
    </div>
  );
}

export default ListarEspaco;
