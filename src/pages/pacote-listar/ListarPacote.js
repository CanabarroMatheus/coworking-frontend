import { useEffect, useState } from 'react';
import Box from '../../components/box/Box';
import AntButton from '../../components/middleware/AntButton';
import { AntModal } from '../../components/middleware/AntModal';
import AntTable from '../../components/middleware/AntTable';
import PacoteForm from '../../components/middleware/Form/PacoteForm';
import { openErrorNotification, openSuccessNotification } from '../../components/middleware/Notifications';
import { tabelaPacote } from '../../constants/tabelas';
import PacoteService from '../../services/pacoteService';
import './listar-pacote.css';

function ListarPacote() {
  const [pacotes, setPacotes] = useState( false );
  const [mostrarModal, setMostrarModal] = useState( false );

  const converterPacote = ( data ) => {
    const pacote = {};
    pacote.valor = data.valor;
    pacote.espacoPacote = [];
    data.espacoPacote.map( espacoPacote => {
      const novoEspaco = {};
      novoEspaco.espaco = {};
      novoEspaco.espaco.id = espacoPacote.espaco;
      novoEspaco.tipoContratacao = espacoPacote.tipoContratacao;
      novoEspaco.quantidade = espacoPacote.quantidade;
      novoEspaco.prazo = espacoPacote.prazo;
      pacote.espacoPacote.push( novoEspaco );
      return true;
    } );
    return pacote;
  }

  const cadastrarPacote = ( data ) => {
    const service = new PacoteService();
    service.novoPacote( converterPacote( data ) )
      .then(() => {
        openSuccessNotification({
          mensagem: 'Sucesso!',
          descricao: 'Pacote cadastrado com sucesso!'
        });
      })
      .catch(() => {
        openErrorNotification({
          mensagem: 'Erro',
          descricao: 'Houve um erro ao tentar cadastrar o pacote, tente novamente mais tarde...'
        });
      })
  }

  useEffect(() => {
    const service = new PacoteService();
    service.trazerTodosPacotes()
      .then( ({ data }) => {
        setPacotes( data );
      } );
  },[pacotes]);

  return (
    <div className="listar-pacote">
      <Box>
        <AntButton tipo="primary" clique={ () => setMostrarModal( true ) }>Cadastrar</AntButton>
      </Box>
      <Box>
        <AntTable data={ pacotes } colunas={ tabelaPacote } />
      </Box>
      <AntModal tamanho titulo="Cadastrar novo pacote" visivel={ mostrarModal } cancelar={ () => setMostrarModal( false ) } cancelarTexto="" okTexto="" botaoOkProps={ { hidden: true } } botaoCancelarProps={ { hidden: true } } >
        <PacoteForm acao={ cadastrarPacote } />
      </AntModal>
    </div>
  );
}

export default ListarPacote;
