import Box from '../../../components/box/Box'
import ContratacaoEspacoForm from '../../../components/middleware/Form/ContratacaoEspacoForm';
import ContratacaoService from '../../../services/contratacaoService';
import PagamentoService from '../../../services/pagamentoService';
import { openErrorNotification, openSuccessNotification } from '../../../components/middleware/Notifications';
import './contratacao-espaco.css'
import { useState } from 'react';
import PagamentoForm from '../../../components/middleware/Form/PagamentoForm';

function ContratacaoEspaco() {
  const [idContratacao, setIdContratacao] = useState();
  const efetuarContratacao = ( { espacoId, clienteId, ...resto } ) => {
    const contratacao = { 
      espaco: { 
        id: espacoId 
      }, 
      cliente: { 
        id: clienteId 
      },
      ...resto
    };
    const service = new ContratacaoService();
    service.novaContratacao( contratacao )
      .then(({ data }) => {
        setIdContratacao( data.id );
        openSuccessNotification({
          mensagem: 'Sucesso!',
          descricao: 'Contratacao efetuada com sucesso!'
        });
      })
      .catch(() => {
        openErrorNotification({
          mensagem: 'Erro',
          descricao: 'Houve um erro ao tentar efetuar a contratação, tente novamente mais tarde...'
        });
      });
  }

  const efetuarPagamento = ( { ...data } ) => {
    const pagamento = { contratacao: { id: idContratacao }, ...data };
    const service = new PagamentoService();
    service.salvarPagamento( pagamento )
      .then(() => {
        openSuccessNotification({
          mensagem: 'Sucesso!',
          descricao: 'Pagamento salvo com sucesso!'
        });
      })
      .catch(() => {
        openErrorNotification({
          mensagem: 'Erro',
          descricao: 'Houve um erro ao tentar efetuar o pagamento, tente novamente mais tarde...'
        });
      });
  }

  return (
    <div className="contratacao-espaco">
      <Box>
        <ContratacaoEspacoForm acao={ ( data ) => efetuarContratacao( data ) } />
        {
          idContratacao && (
            <PagamentoForm acao={ efetuarPagamento }  />
          )
        }
      </Box>
    </div>
  );
}

export default ContratacaoEspaco;