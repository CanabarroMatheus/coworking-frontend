import { useState } from 'react';
import Box from '../../../components/box/Box';
import ContratacaoPacoteForm from '../../../components/middleware/Form/ContratacaoPacoteForm';
import { openErrorNotification, openSuccessNotification } from '../../../components/middleware/Notifications';
import ClientePacoteService from '../../../services/clientePacoteService';
import PagamentoService from '../../../services/pagamentoService';
import PagamentoForm from '../../../components/middleware/Form/PagamentoForm';
import './contratacao-pacote.css';

function ContratacaoPacote( props ) {
  const [idClientePacote, setIdClientePacote] = useState();
  const efetuarContratacao = ({ pacoteId, clienteId, ...resto }) => {
    const clientePacote = {
      pacote: {
        id: pacoteId
      },
      cliente: {
        id: clienteId
      },
      ...resto
    }

    const service = new ClientePacoteService();
    service.salvarClientePacote( clientePacote )
      .then(({ data }) => {
        setIdClientePacote( data.id );
        openSuccessNotification({
          mensagem: 'Sucesso!',
          descricao: 'Contratação do pacote efetuada com sucesso!'
        });
      })
      .catch(() => {
        openErrorNotification({
          mensagem: 'Erro',
          descricao: 'Houve um erro ao tentar efetuar a contratação do pacote, tente novamente mais tarde...'
        });
      });
  }

  const efetuarPagamento = ( { ...data } ) => {
    const pagamento = { clientePacote: { id: idClientePacote }, ...data };
    const service = new PagamentoService();
    service.salvarPagamento( pagamento )
      .then(() => {
        openSuccessNotification({
          mensagem: 'Sucesso!',
          descricao: 'Pagamento salvo com sucesso!'
        });
      })
      .catch(() => {
        openErrorNotification({
          mensagem: 'Erro',
          descricao: 'Houve um erro ao tentar efetuar o pagamento, tente novamente mais tarde...'
        });
      });
  }

  return (
    <div className="contratacao-pacote">
      <Box>
        <ContratacaoPacoteForm acao={ ( data ) => efetuarContratacao( data ) } />
        {
          idClientePacote && (
            <PagamentoForm acao={ efetuarPagamento }  />
          )
        }
      </Box>
    </div>
  );
}

export default ContratacaoPacote;
