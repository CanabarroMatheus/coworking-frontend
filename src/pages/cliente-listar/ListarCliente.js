import { useEffect, useState } from 'react';
import Box from '../../components/box/Box';
import AntButton from '../../components/middleware/AntButton';
import { AntModal } from '../../components/middleware/AntModal';
import AntTable from '../../components/middleware/AntTable';
import ClienteForm from '../../components/middleware/Form/ClienteForm';
import { openErrorNotification, openSuccessNotification } from '../../components/middleware/Notifications';
import { tabelaCliente } from '../../constants/tabelas';
import ClienteService from '../../services/clienteService';
import './listar-cliente.css';

function ListarCliente() {
  const [clientes, setClientes] = useState( false );
  const [mostrarModal, setMostrarModal] = useState( false );

  const converterFormulario = ( data ) => {
    const cliente = {};
    cliente.nome = data.nome;
    cliente.cpf = data.cpf;
    cliente.dataNascimento = data.dataNascimento.format('YYYY-MM-DD');
    cliente.contatos = [];
    data.contatos.map( contato => {
      const novoContato = {};
      novoContato.valor = contato.valor;
      novoContato.tipoContato = JSON.parse( contato.tipoContato );
      cliente.contatos.push( novoContato );
      return true;
    } );

    const service = new ClienteService();
    service.novoCliente( cliente )
      .then(() => {
        openSuccessNotification({
          mensagem: 'Sucesso!',
          descricao: 'Cliente foi cadastrado com sucesso!' 
        });
      })
      .catch(() => {
        openErrorNotification({
          mensagem: 'Erro',
          descricao: 'Houve um erro ao tentar registrar o cliente... Tente novamente mais tarde!' 
        })
      })
  }

  useEffect(() => {
    const service = new ClienteService();
    service.trazerTodosClientes()
      .then( ({ data }) => {
        setClientes( data );
      } );
  },[clientes]);

  return (
    <div className="listar-cliente">
      <Box>
        <AntButton tipo="primary" clique={ () => setMostrarModal( true ) }>Cadastrar</AntButton>
      </Box>
      <Box>
        <AntTable data={ clientes } colunas={ tabelaCliente } paginacao />
      </Box>
      <AntModal titulo="Cadastrar novo cliente" visivel={ mostrarModal } cancelar={ () => setMostrarModal( false ) } cancelarTexto="" okTexto="" botaoOkProps={ { hidden: true } } botaoCancelarProps={ { hidden: true } } >
        <ClienteForm acao={ converterFormulario } />
      </AntModal>
    </div>
  );
}

export default ListarCliente;
