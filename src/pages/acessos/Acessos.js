import Box from '../../components/box/Box';
import AcessoEntradaForm from '../../components/middleware/Form/AcessoEntradaForm';
import AcessoSaidaForm from '../../components/middleware/Form/AcessoSaidaForm';
import { openErrorNotification, openSuccessNotification } from '../../components/middleware/Notifications';
import AcessosService from '../../services/acessosService';
import './acessos.css';

function Acessos() {
  const registrarEntrada = ( data ) => {
    const entrada = {
      saldoCliente: {
        id: {
          id_cliente: data.clienteId,
          id_espaco: data.espacoId
        }
      }
    };

    const service = new AcessosService();
    if ( data.especial ) {
      service.entrarEspecial( entrada )
        .then( () => {
          openSuccessNotification({
            mensagem: 'Sucesso!',
            descricao: 'Entrada registrada com sucesso.'
          });
        })
        .catch(() => {
          openErrorNotification({
            mensagem: 'Erro',
            descricao: 'Cheque se o cliente registrou entrada anteriormente e tente novamente...'
          })
        });
    } else {
      service.entrar( entrada )
        .then( () => {
          openSuccessNotification({
            mensagem: 'Sucesso!',
            descricao: 'Entrada registrada com sucesso.'
          });
        })
        .catch(() => {
          openErrorNotification({
            mensagem: 'Erro',
            descricao: 'Cheque se o cliente registrou entrada anteriormente e tente novamente...'
          })
        });
    }
  }

  const registrarSaida = ( data ) => {
    const saida = {
      saldoCliente: {
        id: {
          id_cliente: data.clienteId,
          id_espaco: data.espacoId
        }
      }
    };
    const service = new AcessosService();
    service.sair( saida )
      .then( () => {
        openSuccessNotification({
          mensagem: 'Sucesso!',
          descricao: 'Entrada registrada com sucesso.'
        });
      })
      .catch(() => {
        openErrorNotification({
          mensagem: 'Erro',
          descricao: 'Cheque se o cliente registrou saída anteriormente e tente novamente...'
        })
      });
  }

  return (
    <div className="acessos">
      <Box>
        <AcessoEntradaForm acao={ data => registrarEntrada( data ) } />
      </Box>

      <Box>
        <AcessoSaidaForm acao={ data => registrarSaida( data ) } />
      </Box>
    </div>
  );
}

export default Acessos;
