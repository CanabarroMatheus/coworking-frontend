import { useEffect, useState } from 'react';
import Box from '../../components/box/Box'
import AntButton from '../../components/middleware/AntButton'
import { AntModal } from '../../components/middleware/AntModal';
import AntTable from '../../components/middleware/AntTable'
import { tabelaTipoContato } from '../../constants/tabelas';
import TipoContatoService from '../../services/tipoContatoService';
import { openSuccessNotification, openErrorNotification } from '../../components/middleware/Notifications';
import './tipo-contato.css'
import TipoContatoForm from '../../components/middleware/Form/TipoContatoForm';

function TipoContato() {

  const [tipoContatos, setTipoContatos] = useState( false );
  const [mostrarModal, setMostrarModal] = useState( false );

  useEffect(() => {
    const service = new TipoContatoService();
    service.trazerTodosTipoContato()
      .then( ({ data }) => {
        setTipoContatos( data );
      } );
  },[tipoContatos]);

   const cadastrarTipoContato = ( data ) => {
    const service = new TipoContatoService();
    service.novoTipoContato( data ).then(() => {
      openSuccessNotification({
        mensagem: 'Sucesso!',
        descricao: 'Tipo de contato foi cadastrado com sucesso!' 
      });
      setMostrarModal( false );
    })
    .catch(() => {
      openErrorNotification({
        mensagem: 'Erro',
        descricao: 'Houve um erro ao tentar registrar o tipo de contato, tente novamente mais tarde...' 
      });
    });    
  }

  return (
    <div className="tipo-contato">
      <Box>
        <AntButton tipo="primary" clique={ () => setMostrarModal( true ) }>Cadastrar</AntButton>
      </Box>
      <Box>
        <AntTable data={ tipoContatos } colunas={ tabelaTipoContato } paginacao={ false } />
      </Box>
      <AntModal titulo="Cadastrar novo tipo de contato" visivel={ mostrarModal } cancelar={ () => setMostrarModal( false ) } cancelarTexto="Cancelar" okTexto="Cadastrar" botaoOkProps={ { hidden: true } } botaoCancelarProps={ { hidden: true } } >
        <TipoContatoForm acao={ cadastrarTipoContato } />
      </AntModal>
    </div>
  );
}

export default TipoContato;