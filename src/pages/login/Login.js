import React from 'react';

import LoginBox from '../../components/login/LoginBox';

import './login.css';

function Login() {
  return (
    <div className="login">
      <LoginBox />
    </div>
  );
}

export default Login;