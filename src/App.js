import React from 'react';
import { BrowserRouter as Router, Redirect, Route } from 'react-router-dom';
import { verificarLogin } from './helpers/storage';

import Acessos from './pages/acessos/Acessos';
import ListarCliente from './pages/cliente-listar/ListarCliente';
import TipoContato from './pages/tipo-contato/TipoContato';
import Login from './pages/login/Login';
import Home from './pages/home/Home'
import AntBaseLayout from './components/middleware/AntBaseLayout';

import './assets/css/reset.css';
import 'antd/dist/antd.css';
import ListarEspaco from './pages/espaco-listar/ListarEspaco';
import ListarPacote from './pages/pacote-listar/ListarPacote';
import ContratacaoEspaco from './pages/contratacao/espaco/ContratacaoEspaco';
import ContratacaoPacote from './pages/contratacao/pacote/ContratacaoPacote';

function App() {
  const PrivateRoute = ({ component: Component, ...rest }) => {
    return <Route { ...rest } render={ ( props ) => (
      verificarLogin() 
      ? (
        <AntBaseLayout>
          <Component { ...props } />
        </AntBaseLayout>
      )
      : <Redirect to="/login" />
    ) } />
  }

  return (
    <Router>
      <PrivateRoute path="/" exact component={ Home } />
      <PrivateRoute path="/home" exact component={ Home } />
      <PrivateRoute path="/cliente/tipo-contato" exact component={ TipoContato } />
      <PrivateRoute path="/cliente/listar" exact component={ ListarCliente } />
      <PrivateRoute path="/espaco/listar" exact component={ ListarEspaco } />
      <PrivateRoute path="/pacote/listar" exact component={ ListarPacote } />
      <PrivateRoute path="/contratacao" exact component={ ContratacaoEspaco } />
      <PrivateRoute path="/contratacao-pacote" exact component={ ContratacaoPacote } />
      <PrivateRoute path="/acessos" exact component={ Acessos } />
      <Route path="/login" exact component={ Login } />
    </Router>
  );
}

export default App;
