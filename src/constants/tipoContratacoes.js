const TipoContratacoes = [
  { nome: 'Minutos', valor: 'MINUTOS' },
  { nome: 'Horas', valor: 'HORAS' },
  { nome: 'Turnos', valor: 'TURNOS' },
  { nome: 'Diarias', valor: 'DIARIAS' },
  { nome: 'Semanas', valor: 'SEMANAS' },
  { nome: 'Meses', valor: 'MESES' },
  { nome: 'Especial', valor: 'ESPECIAL' }
]

export default TipoContratacoes;
