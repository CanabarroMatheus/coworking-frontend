const MenuItens = [
  {
    ORDEM: 1,
    TITULO: 'Clientes',
    ICONE: 'TeamOutlinedIcon',
    ITENS: [
      {
        ORDEM: 1,
        TITULO: 'Listar clientes',
        ICONE: 'BarsOutlinedIcon',
        LINK: '/cliente/listar'
      },
      {
        ORDEM: 2,
        TITULO: 'Tipos de contato',
        ICONE: 'PhoneOutlinedIcon',
        LINK: '/cliente/tipo-contato',
        ROTACAO: 90,
      }
    ]
  },
  {
    ORDEM: 2,
    TITULO: 'Espaços',
    ICONE: 'DesktopOutlinedIcon',
    ITENS: [
      {
        ORDEM: 3,
        TITULO: 'Listar espaços',
        ICONE: 'BarsOutlinedIcon',
        LINK: '/espaco/listar'
      }
    ]
  },
  {
    ORDEM: 3,
    TITULO: 'Pacotes',
    ICONE: 'InboxOutlinedIcon',
    ITENS: [
      {
        ORDEM: 4,
        TITULO: 'Listar pacotes',
        ICONE: 'BarsOutlinedIcon',
        LINK: '/pacote/listar'
      }
    ]
  },
  {
    ORDEM: 4,
    TITULO: 'Contratações',
    ICONE: 'DollarCircleOutlinedIcon',
    ITENS: [
      {
        ORDEM: 5,
        TITULO: 'Contratação de espaço',
        ICONE: 'DollarCircleOutlinedIcon',
        LINK: '/contratacao'
      },
      {
        ORDEM: 6,
        TITULO: 'Contratação de pacote',
        ICONE: 'DollarCircleOutlinedIcon',
        LINK: '/contratacao-pacote'
      }
    ]
  },
  {
    ORDEM: 5,
    TITULO: 'Acessos',
    ICONE: 'BookOutlinedIcon',
    ITENS: [
      {
        ORDEM: 7,
        TITULO: 'Monitorar',
        ICONE: 'EyeOutlinedIcon',
        LINK: '/acessos'
      }
    ]
  }
]

export default MenuItens;
