const TipoPagamento = [
  { NOME: 'Débito', VALOR: 'DEBITO' },
  { NOME: 'Crédito', VALOR: 'CREDITO' }
]

export default TipoPagamento;
