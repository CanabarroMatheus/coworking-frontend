import AntTag from '../components/middleware/AntTag';

export const tabelaTipoContato = [
  { title: 'Id', dataIndex: 'id', key: 'id', render: texto => <b>{ texto }</b> },
  { title: 'Nome', dataIndex: 'nome', key: 'nome' }
];

export const tabelaCliente = [
  { title: 'Id', dataIndex: 'id', key: 'id', render: texto => <b>{ texto }</b> },
  { title: 'Nome', dataIndex: 'nome', key: 'nome' },
  { title: 'CPF', dataIndex: 'cpf', key: 'cpf' },
  { title: 'Data de nascimento', dataIndex: 'dataNascimento', key: 'dataNascimento', render: data => <span>{ new Date( data ).toLocaleDateString('pt-BR') }</span> }
];

export const tabelaEspaco = [
  { title: 'Id', dataIndex: 'id', key: 'id', render: texto => <b>{ texto }</b> },
  { title: 'Nome', dataIndex: 'nome', key: 'nome' },
  { title: 'Quantidade de pessoas', dataIndex: 'quantidadePessoas', key: 'quantidadePessoas' },
  { title: 'Valor', dataIndex: 'valor', key: 'valor' }
];

export const tabelaPacote = [
  { title: 'Id', dataIndex: 'id', key: 'id', render: texto => <b>{ texto }</b> },
  { title: 'Valor', dataIndex: 'valor', key: 'valor' },
  { title: 'Espaços', dataIndex: 'espacoPacote', key: '1', render: espacoPacote => (
    <>
      { espacoPacote.map( ({ espaco, tipoContratacao }) => <AntTag cor="blue">{ `${ espaco.nome } - ${ tipoContratacao }` }</AntTag> ) }
    </>
  ) },
]