import * as Icons from '../components/middleware/Icons';

export function converterStringParaIcone( input ) {
  const stringIcone = input.ICONE;
  switch ( stringIcone ) {
    case 'UserOutlineIcon':
      input.ICONE = Icons.IconUserOutline;
      break;
    
    case 'LockOutlineIcon':
      input.ICONE = Icons.IconLockOutline;
      break;
    
    case 'BarsOutlinedIcon':
      input.ICONE = Icons.IconBarsOutlined;
      break;
  
    case 'SearchOutlinedIcon':
      input.ICONE = Icons.IconSearchOutlined;
      break;

    case 'TeamOutlinedIcon':
      input.ICONE = Icons.IconTeamOutlined;
      break;

    case 'DesktopOutlinedIcon':
      input.ICONE = Icons.IconDesktopOutlined;
      break;
    
    case 'InboxOutlinedIcon':
      input.ICONE = Icons.IconInboxOutlined;
      break;

    case 'PhoneOutlinedIcon':
      input.ICONE = Icons.IconPhoneOutlined;
      break;

    case 'HomeOutlinedIcon':
      input.ICONE = Icons.IconHomeOutlined;
      break;

    case 'DollarCircleOutlinedIcon':
      input.ICONE = Icons.IconDollarCircleOutlined;
      break;

    case 'BookOutlinedIcon':
      input.ICONE = Icons.IconBookOutlined;
      break;

    case 'EyeOutlinedIcon':
      input.ICONE = Icons.IconEyeOutlined;
      break;

    default:
      break;
  }

  return input;
}