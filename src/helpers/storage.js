const STORAGE_KEY = 'coworking-storage-key';

export const salvarDataNoStorage = ( data ) => {
  try {
    sessionStorage.setItem( STORAGE_KEY, JSON.stringify( data ) );
    return true;
  } catch ( error ) {
    return false;
  }
}

export const obterLoginDataDoStorage = () => {
  try {
    const data = JSON.parse( sessionStorage.getItem( STORAGE_KEY ) );

    return data;
  } catch ( err ) {
    return console.error( err );;
  }
}

export const verificarLogin = () => {
  try {
    const loginData = JSON.parse( sessionStorage.getItem( STORAGE_KEY ) );
    if ( loginData ) {
      return true;
    }
    return false;
  } catch ( err ) {
    console.error( err );
  }
}