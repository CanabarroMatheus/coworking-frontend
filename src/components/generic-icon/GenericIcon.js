function GenericIcon( props ) {
  const { component: Icon } = props;

  return <Icon { ...props } />
}

export default GenericIcon;
