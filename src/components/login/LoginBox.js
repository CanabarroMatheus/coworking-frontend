import './login-box.css';
import { useHistory } from 'react-router';

import LoginService from '../../services/loginService';
import { salvarDataNoStorage } from '../../helpers/storage';
import LoginForm from '../middleware/Form/LoginForm';
import RegistroForm from '../middleware/Form/RegistroForm';
import AntButton from '../middleware/AntButton';
import { useState } from 'react';
import RegistroService from '../../services/registroService';

function LoginBox() {
  const history = useHistory();
  const [login, setLogin] = useState( true );

  const loginRegistro = ( verificador ) => verificador 
    ? <LoginForm acao={ logar } /> 
    : <RegistroForm acao={ registrar } />;

  const logar = ( data ) => {
    new LoginService().login( data )
      .then( response => {
        salvarDataNoStorage( response.headers.authorization );
        history.push( '/home' );
      } );
  }

  const registrar = ( data ) => {
    new RegistroService().registrar( data )
      .then(() => {
        const login = { login: data.login, senha: data.senha };
        logar( login );
      })
  }

  return (
    <div className="login-box">
      <h1>Login</h1>
      { loginRegistro( login ) }
      <AntButton tipo="link" clique={ () => setLogin( !login ) }>{ login ? 'Registrar-se?' : 'Fazer login?' }</AntButton>
    </div>
  );
}

export default LoginBox;
