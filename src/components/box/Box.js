import './box.css';

function Box( { children } ) {
  return (
    <div className={ `box-content` }>
      { children }
    </div>
  );
}

export default Box;
