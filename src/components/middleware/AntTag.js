import { Tag } from 'antd';

function AntTag( props ) {
  const { cor, children } = props;
  return (
    <Tag color={ cor }>{ children }</Tag>
  );
}

export default AntTag;
