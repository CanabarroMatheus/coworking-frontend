import {
  HomeOutlined,
  UserOutlined, 
  LockOutlined, 
  MenuUnfoldOutlined, 
  MenuFoldOutlined, 
  BarsOutlined, 
  SearchOutlined, 
  TeamOutlined, 
  DesktopOutlined, 
  InboxOutlined,
  PhoneOutlined,
  EditOutlined,
  MinusCircleOutlined,
  PlusCircleOutlined,
  DollarCircleOutlined,
  BookOutlined,
  EyeOutlined
} from '@ant-design/icons';

export function IconHomeOutlined( props ) {
  return <HomeOutlined className={ props.classe } rotate={ props.rotacao } spin={ props.girar } onClick={ props.clicar } />
}

export function IconUserOutline( props ) {
  return <UserOutlined className={ props.classe } rotate={ props.rotacao } spin={ props.girar } />
}

export function IconLockOutline( props ) {
  return <LockOutlined className={ props.classe } rotate={ props.rotacao } spin={ props.girar } />;
}

export function IconMenuUnfoldOutlined( props ) {
  return <MenuUnfoldOutlined className={ props.classe } rotate={ props.rotacao } spin={ props.girar } onClick={ props.clicar } />
}

export function IconMenuFoldOutlined( props ) {
  return <MenuFoldOutlined className={ props.classe } rotate={ props.rotacao } spin={ props.girar } onClick={ props.clicar } />
}

export function IconBarsOutlined( props ) {
  return <BarsOutlined className={ props.classe } rotate={ props.rotacao } spin={ props.girar } onClick={ props.clicar } />
}

export function IconSearchOutlined( props ) {
  return <SearchOutlined className={ props.classe } rotate={ props.rotacao } spin={ props.girar } onClick={ props.clicar } />
}

export function IconTeamOutlined( props ) {
  return <TeamOutlined className={ props.classe } rotate={ props.rotacao } spin={ props.girar } onClick={ props.clicar } />
}

export function IconDesktopOutlined( props ) {
  return <DesktopOutlined className={ props.classe } rotate={ props.rotacao } spin={ props.girar } onClick={ props.clicar } />
}

export function IconInboxOutlined( props ) {
  return <InboxOutlined className={ props.classe } rotate={ props.rotacao } spin={ props.girar } onClick={ props.clicar } />
}

export function IconPhoneOutlined( props ) {
  return <PhoneOutlined className={ props.classe } rotate={ props.rotacao } spin={ props.girar } onClick={ props.clicar } />
}

export function IconEditOutlined( props ) {
  return <EditOutlined className={ props.classe } rotate={ props.rotacao } spin={ props.girar } onClick={ props.clicar } />
}

export function IconMinusCircleOutlined( props ) {
  return <MinusCircleOutlined className={ props.classe } rotate={ props.rotacao } spin={ props.girar } onClick={ props.clicar } />
}

export function IconPlusCircleOutlined( props ) {
  return <PlusCircleOutlined className={ props.classe } rotate={ props.rotacao } spin={ props.girar } onClick={ props.clicar } />
}

export function IconDollarCircleOutlined( props ) {
  return <DollarCircleOutlined className={ props.classe } rotate={ props.rotacao } spin={ props.girar } onClick={ props.clicar } />
}

export function IconBookOutlined( props ) {
  return <BookOutlined className={ props.classe } rotate={ props.rotacao } spin={ props.girar } onClick={ props.clicar } />;
}

export function IconEyeOutlined( props ) {
  return <EyeOutlined className={ props.classe } rotate={ props.rotacao } spin={ props.girar } onClick={ props.clicar } />;
}
