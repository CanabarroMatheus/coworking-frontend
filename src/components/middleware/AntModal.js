import Modal from 'antd/lib/modal/Modal';

export function AntModal( props ) {
  const { titulo, tamanho, visivel, ok, cancelar, okTexto, cancelarTexto, botaoOkProps, botaoCancelarProps, children } = props;

  return (
    <Modal title={ titulo } width={ tamanho } visible={ visivel } onOk={ ok } onCancel={ cancelar } okText={ okTexto } cancelText={ cancelarTexto } okButtonProps={ botaoOkProps } cancelButtonProps={ botaoCancelarProps } >
      { children }
    </Modal>
  );
}