import { Layout } from 'antd';
import AntSider from './AntSider';
import MenuItens from '../../constants/menuItens';
import { useState } from 'react';

import '../../assets/css/layout.css';

function AntBaseLayout( props ) {
  const { Content, Footer } = Layout;
  const { children } = props;

  const [colapsar, setColapsar] = useState( true );

  return (
    <Layout className="coworking-layout">
      <AntSider tema="dark" modo="inline" itens={ MenuItens } colapsavel colapsado={ colapsar } colapsar={ () => setColapsar( !colapsar ) } />
      <Layout>
        <Content>
          { children }
        </Content>
        <Footer>Matheus Canabarro @2021 Criado por Matheus Canabarro</Footer>
      </Layout>
    </Layout>
  );
}

export default AntBaseLayout;
