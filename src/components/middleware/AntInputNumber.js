import { InputNumber } from 'antd';

function AntInputNumber( props ) {
  const { nome, tamanho, placeholder, minimo, maximo, prefixo, formatador, parseador, desabilitado, borda } = props;
  return <InputNumber name={ nome } size={ tamanho } placeholder={ placeholder } min={ minimo } max={ maximo } prefix={ prefixo } formatter={ formatador } parser={ parseador } disabled={ desabilitado } bordered={ borda } />
}

export default AntInputNumber;