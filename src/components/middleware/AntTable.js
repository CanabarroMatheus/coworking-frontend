import { Table } from 'antd';

function AntTable( { data, colunas, titulo, rodape, expandivel, paginacao } ) {
  return <Table dataSource={ data } columns={ colunas } title={ titulo } footer={ rodape } expandable={ expandivel } pagination={ paginacao } />
}

export default AntTable;
