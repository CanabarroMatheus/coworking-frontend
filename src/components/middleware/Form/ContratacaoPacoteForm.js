import { useEffect, useState } from 'react';
import { Col, Form, Button, InputNumber, Row, Select } from 'antd';
import ClienteService from '../../../services/clienteService';
import PacoteService from '../../../services/pacoteService';

function ContratacaoPacoteForm( props ) {
  const { acao } = props;
  const { Option } = Select;
  const [pacotes, setPacotes] = useState([]);
  const [clientes, setClientes] = useState([]);

  const adicionarOptionPacote = pacoteOption => <Option value={ pacoteOption.id }>{ pacoteOption.valor }</Option>;
  const adicionarOptionCliente = clienteOption => <Option value={ clienteOption.id }>{ clienteOption.nome }</Option>

  useEffect(() => {
    const pacoteService = new PacoteService();
    const clienteService = new ClienteService();

    Promise.all(
      [pacoteService.trazerTodosPacotes(), clienteService.trazerTodosClientes()]
    ).then(( responses ) => {
      setPacotes( responses[0].data );
      setClientes( responses[1].data );
    });
  }, []);

  return (
    <Form layout="horizontal" name="clientePacote" onFinish={ acao }>
      <h1>Contratação</h1>
      <Row gutter={ 8 }>
        <Col span={ 6 }>
          <Form.Item className="contratacao-input" label="Pacote" name="pacoteId" rules={ [{ required: true, message: 'Campo obrigatório' }] }>
            <Select>
              { pacotes.map( ( pacoteMap ) => adicionarOptionPacote( pacoteMap ) ) }
            </Select>
          </Form.Item>
        </Col>
        <Col span={ 6 }>
          <Form.Item className="contratacao-input" label="Cliente" name="clienteId" rules={ [{ required: true, message: 'Campo obrigatório' }] }>
            <Select>
              { clientes.map( ( clienteMap ) => adicionarOptionCliente( clienteMap ) ) }
            </Select>
          </Form.Item>
        </Col>
        <Col span={ 6 }>
          <Form.Item label="Quantidade" name="quantidade" rules={ [{ required: true, message: 'Campo obrigatório' }] }>
            <InputNumber className="contratacao-input" min="1" minLength="1" />
          </Form.Item>
        </Col>
        <Col span={ 6 }>
          <Button block className="submit" type="primary" htmlType="submit">Efetuar contratacão</Button>
        </Col>
      </Row>
    </Form>
  );
}

export default ContratacaoPacoteForm;
