import { Form, Input, InputNumber, Select, Space, Button } from 'antd';
import { useEffect, useState } from 'react';
import EspacoService from '../../../services/espacoService';
import TipoContratacoes from '../../../constants/tipoContratacoes';
import { IconMinusCircleOutlined, IconPlusCircleOutlined } from '../Icons';

function PacoteForm( props ) {
  const { acao } = props;
  const { Option } = Select;
  const [espacos, setEspacos] = useState( false );

  useEffect(() => {
    const service = new EspacoService();
    service.trazerTodosEspacos()
      .then( ({ data }) => {
        setEspacos( data );
      })
  }, []);

  const adicionarOptionEspaco = espaco => {
    return <Option value={ espaco.id } >{ espaco.nome }</Option>
  }

  const adicionarOptionTipoContratacao = tipoContratacao => {
    return <Option value={ tipoContratacao.valor } >{ tipoContratacao.nome }</Option>
  }

  return (
    <Form layout="vertical" name="pacote" onFinish={ acao }>
      <Form.Item label="Valor" name="valor" rules={ [{ required: true, message: 'Campo obrigatório' }] } >
        <Input />
      </Form.Item>

      <Form.List name="espacoPacote">
        { ( fields, { add, remove } ) => (
          <>
            { fields.map( ({ key, name, fieldKey, ...RestField }) => (
              <Space className="space" key={ key }>
                <Form.Item className="pacote-input" label="Espaço" { ...RestField } name={ [name, 'espaco'] } fieldKey={ [name, 'espaco'] } rules={ [{ required: true, message: 'Campo obrigatório!' }] } >
                  <Select className="pacote-input">
                    { espacos.map( espaco => adicionarOptionEspaco( espaco ) ) }
                  </Select>
                </Form.Item>
                
                <Form.Item className="pacote-input" label="Tipo de contratação" { ...RestField } name={ [name, 'tipoContratacao'] } fieldKey={ [name, 'tipoContratacao'] } rules={ [{ required: true, message: 'Campo obrigatório!' }] } >
                  <Select>
                    { TipoContratacoes.map( tipoContratacao => adicionarOptionTipoContratacao( tipoContratacao ) ) }
                  </Select>
                </Form.Item>
                
                <Form.Item label="Quantidade" { ...RestField } name={ [name, 'quantidade'] } fieldKey={ [name, 'quantidade'] } rules={ [{ required: true, message: 'Campo obrigatório!' }] } >
                  <InputNumber className="pacote-input" min="1" minLength="1" />
                </Form.Item>
                
                <Form.Item label="Prazo" { ...RestField } name={ [name, 'prazo'] } fieldKey={ [name, 'prazo'] } rules={ [{ required: true, message: 'Campo obrigatório!' }] } >
                  <InputNumber className="pacote-input" min="1" minLength="1" />
                </Form.Item>

                <IconMinusCircleOutlined clicar={ () => remove( name ) } />
              </Space>
            ) ) }
            <Form.Item>
              <Button type="primary" onClick={ () => add() } block icon={ <IconPlusCircleOutlined /> }>
                Adicionar espaço
              </Button>
            </Form.Item>
          </>
        ) }
      </Form.List>

      <Button className="submit" type="primary" htmlType="submit" >Cadastrar</Button>
    </Form>
  );
}

export default PacoteForm;
