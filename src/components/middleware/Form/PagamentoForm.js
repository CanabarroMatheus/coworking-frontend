import { Col, Form, Row, Select, Button } from 'antd';
import TipoPagamento from '../../../constants/tipoPagamento';

function PagamentoForm( props ) {
  const { acao } = props;
  const { Option } = Select;

  const adicionarOptionTipoPagamento = ( tipoPagamento ) => <Option value={ tipoPagamento.VALOR }>{ tipoPagamento.NOME }</Option>;

  return (
    <Form layout="horizontal" name="pagamento" onFinish={ acao }>
      <h1>Pagamento</h1>
      <Row>
        <Col span={ 22 }>
          <Form.Item label="Tipo de pagamento" name="tipoPagamento" rules={ [{ required: true, message: 'Campo obrigatório' }] }>
            <Select>
              { TipoPagamento.map( tipoPagamento => adicionarOptionTipoPagamento( tipoPagamento ) ) }
            </Select>
          </Form.Item>
        </Col>

        <Col span={ 2 }>
          <Button className="submit" type="primary" htmlType="submit">Efetuar pagamento</Button>
        </Col>
      </Row>
    </Form>
  );
}

export default PagamentoForm;
