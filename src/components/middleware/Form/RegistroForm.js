import { Col, Form, Input, Row, Button } from 'antd';

function RegistroForm( props ) {
  const { acao } = props;
  return(
    <Form layout="vertical" name="registro" onFinish={ acao }>
      <Row gutter={ 16 }>
        <Col span={ 12 }>
          <Form.Item label="Nome" name="nome" rules={ [{ required: true, message: 'Campo obrigatório!' }] }>
            <Input />
          </Form.Item>
        </Col>

        <Col span={ 12 }>
          <Form.Item label="Nome de usuário" name="login" rules={ [{ required: true, message: 'Campo obrigatório!' }] }>
            <Input />
          </Form.Item>
        </Col>
      </Row>
      <Row>
        <Col span={ 24 }>
          <Form.Item label="E-Mail" name="email" rules={ [{ required: true, message: 'Campo obrigatório!' }] }>
            <Input type="email"/>
          </Form.Item>
        </Col>
      </Row>
      <Row>
        <Col span={ 24 }>
          <Form.Item label="Senha" name="senha" rules={ [{ required: true, message: 'Campo obrigatório!' }] }>
            <Input.Password />
          </Form.Item>
        </Col>
      </Row>

      <Button className="submit" type="primary" htmlType="submit">Registrar</Button>
    </Form>
  );
}

export default RegistroForm;
