import { Form, Input, InputNumber, Button } from 'antd';

function EspacoForm( props ) {
  const { acao } = props;
  return(
    <Form layout="vertical" name="espaco" onFinish={ acao }>
      <Form.Item label="Nome" name="nome" rules={ [{ required: true, message: 'Campo obrigatório' }] } >
        <Input />
      </Form.Item>
      
      <Form.Item label="Quantidade de pessoas" name="quantidadePessoas" rules={ [{ required: true, message: 'Campo obrigatório' }] } >
        <InputNumber min="1" minLength="1" />
      </Form.Item>

      <Form.Item label="Valor" name="valor" rules={ [{ required: true, message: 'Campo obrigatório' }] } >
        <Input />
      </Form.Item>

      <Button className="submit" type="primary" htmlType="submit" >Cadastrar</Button>
    </Form>
  );
}

export default EspacoForm;
