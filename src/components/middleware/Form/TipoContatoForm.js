import { Button, Form, Input } from 'antd';

function TipoContatoForm( props ) {
  const { acao } = props;
  return (
    <Form layout="horizontal" name="tipoContato" onFinish={ acao }>
      <Form.Item label="Nome" name="nome" rules={ [{ required: true, message: 'Campo obrigatório' }] }>
        <Input placeholder="Ex.: Telefone, E-Mail, Skype..." />
      </Form.Item>

      <Button className="submit" type="primary" htmlType="submit">Cadastrar</Button>
    </Form>
  );
}

export default TipoContatoForm;
