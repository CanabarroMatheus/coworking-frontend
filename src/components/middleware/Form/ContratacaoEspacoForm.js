import { Button, Col, Form, InputNumber, Row, Select, Input } from 'antd';
import { useEffect, useState } from 'react';
import EspacoService from '../../../services/espacoService';
import ClienteService from '../../../services/clienteService';
import TipoContratacoes from '../../../constants/tipoContratacoes';

function ContratacaoEspacoForm( props ) {
  const { acao } = props;
  const { Option } = Select;
  const [espacos, setEspaco] = useState( [] );
  const [clientes, setCliente] = useState( [] );

  const adicionarOptionEspaco = espacoOption => <Option value={ espacoOption.id }>{ espacoOption.nome }</Option>;
  const adicionarOptionCliente = clienteOption => <Option value={ clienteOption.id }>{ clienteOption.nome }</Option>;
  const adicionarOptionTipoContratacao = tipoContratacaoOption => <Option value={ tipoContratacaoOption.valor }>{ tipoContratacaoOption.nome }</Option>;

  useEffect(() => {
    const espacoService = new EspacoService();
    const clienteService = new ClienteService();

    Promise.all(
      [espacoService.trazerTodosEspacos(), clienteService.trazerTodosClientes()]
    ).then(( responses ) => {
      setEspaco( responses[0].data );
      setCliente( responses[1].data );
    });
    
  },[]);

  return (
    <Form layout="vertical" name="contratacao" onFinish={ acao }>
      <h1>Contratação</h1>
      <Row gutter={16}>
        <Col span={12}>
          <Form.Item className="contratacao-input" label="Espaço" name="espacoId" rules={ [{ required: true, message: 'Campo obrigatório' }] }>
            <Select>
              { espacos.map( ( espacoMap ) => adicionarOptionEspaco( espacoMap ) ) }
            </Select>
          </Form.Item>
        </Col>

        <Col span={12}>
          <Form.Item className="contratacao-input" label="Cliente" name="clienteId" rules={ [{ required: true, message: 'Campo obrigatório' }] }>
            <Select>
              { clientes.map( ( clienteMap ) => adicionarOptionCliente( clienteMap ) ) }
            </Select>
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={16}>
        <Col span={6}>
          <Form.Item className="contratacao-input" label="Tipo de contratação" name="tipoContratacao" >
            <Select>
              { TipoContratacoes.map( ( tipoContratacaoMap ) => adicionarOptionTipoContratacao( tipoContratacaoMap ) ) }
            </Select>
          </Form.Item>
        </Col>
        
        <Col span={6}>
          <Form.Item label="Quantidade" name="quantidade" rules={ [{ required: true, message: 'Campo obrigatório' }] }>
            <InputNumber className="contratacao-input" min="1" minLength="1" />
          </Form.Item>
        </Col>
        
        <Col span={6}>
          <Form.Item label="Prazo" name="prazo" rules={ [{ required: true, message: 'Campo obrigatório' }] }>
            <InputNumber className="contratacao-input" min="1" minLength="1" />
          </Form.Item>
        </Col>
        
        <Col span={6}>
          <Form.Item label="Desconto" name="desconto">
            <Input addonAfter="%" className="contratacao-input" />
          </Form.Item>
        </Col>
      </Row>

      <Button block className="submit" type="primary" htmlType="submit">Efetuar contratacão</Button>
    </Form>
  );
}

export default ContratacaoEspacoForm;
