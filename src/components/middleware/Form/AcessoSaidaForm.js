import { Col, Form, Row, Select, Button } from 'antd';
import { useEffect, useState } from 'react';
import ClienteService from '../../../services/clienteService';
import EspacoService from '../../../services/espacoService';

function AcessoSaidaForm( props ) {
  const { acao } = props;
  const { Option } = Select;
  const adicionarOptionCliente = ( cliente ) => <Option value={ cliente.id }>{ cliente.nome }</Option>;
  const adicionarOptionEspaco = ( espaco ) => <Option value={ espaco.id }>{ espaco.nome }</Option>;
  const [clientes, setClientes] = useState( [] );
  const [espacos, setEspacos] = useState( [] );

  useEffect(() => {
    const clienteService = new ClienteService();
    const espacoService = new EspacoService();
    Promise.all(
      [clienteService.trazerTodosClientes(), espacoService.trazerTodosEspacos()]
    ).then(( responses ) => {
      setClientes( responses[0].data );
      setEspacos( responses[1].data )
    });
  },[]);

  return (
    <Form layout="horizontal" onFinish={ acao }>
      <h1>Saída</h1>

      <Row gutter={ 24 }>
        <Col span={ 6 }>
          <Form.Item label="Cliente" name="clienteId">
            <Select>
              { clientes.map( cliente => adicionarOptionCliente( cliente ) ) }
            </Select>
          </Form.Item>
        </Col>
        <Col span={ 6 }>
          <Form.Item label="Espaco" name="espacoId">
            <Select>
              { espacos.map( espaco => adicionarOptionEspaco( espaco ) ) }
            </Select>
          </Form.Item>
        </Col>
      </Row>
      
      <Button block className="submit" type="primary" htmlType="submit" >Registrar saída</Button>
    </Form>
  );
}

export default AcessoSaidaForm;
