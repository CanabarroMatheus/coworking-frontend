import { Button, Form, Input } from 'antd';
import { IconUserOutline, IconLockOutline } from '../Icons';

function LoginForm( props ) {
  const { acao } = props;
  return (
    <Form layout="horizontal" name="login" onFinish={ acao } >
      <Form.Item label="Login" name="login" rules={ [{ required: true, message: 'Campo obrigatório!' }] }>
        <Input prefix={ <IconUserOutline /> } />
      </Form.Item>

      <Form.Item label="Senha" name="senha" rules={ [{ required: true, message: 'Campo obrigatório!' }] }>
        <Input.Password prefix={ <IconLockOutline /> } />
      </Form.Item>

      <Button className="submit" type="primary" htmlType="submit">Entrar</Button>
    </Form>
  );
}

export default LoginForm;