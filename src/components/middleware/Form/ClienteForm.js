import { Button, DatePicker, Form, Input, Select, Space } from 'antd';
import { useEffect, useState } from 'react';
import { IconMinusCircleOutlined, IconPlusCircleOutlined } from '../Icons';
import TipoContatoService from '../../../services/tipoContatoService';

function ClienteForm( props ) {
  const { acao } = props;
  const { Option } = Select;
  const [tiposContato, setTiposContatos] = useState( false );

  useEffect(() => {
    const service = new TipoContatoService();
    service.trazerTodosTipoContato()
      .then( ({ data }) => setTiposContatos( data ) );
  },[]);

  const adicionarOption = tipoContato => {
    return <Option value={ `{"id":${ tipoContato.id },"nome":"${ tipoContato.nome }"}` }>{ tipoContato.nome }</Option>
  }

  return (
    <Form layout="vertical" name="cliente" onFinish={ acao }>
      <Form.Item label="Nome" name="nome" rules={ [{ required: true, message: 'Campo obrigatório' }] }>
        <Input />
      </Form.Item>
      
      <Form.Item label="CPF" name="cpf" rules={ [{ required: true, message: 'Campo obrigatório' }] }>
        <Input minLength="11" maxLength="11" />
      </Form.Item>
      
      <Form.Item label="Data de nascimento" name="dataNascimento" rules={ [{ required: true, message: 'Campo obrigatório' }] }>
        <DatePicker picker="date" />
      </Form.Item>

      <Form.List name="contatos">
        { ( fields, { add, remove } ) => (
          <>
            { fields.map( ({ key, name, fieldKey, ...restField }) => (
              <Space size="large" key={ key } align="baseline">
                <Form.Item label="Contato" { ...restField } name={ [name, 'valor'] } fieldKey={ [name, 'valor'] } rules={ [{ required: true, message: 'Campo obrigatório!' }] } >
                  <Input />
                </Form.Item>

                <Form.Item label="Tipo de contato" name={ [name, 'tipoContato'] } fieldKey={ [name, 'tipoContato'] } rules={ [{ required: true, message: 'Campo obrigatório!' }] } >
                  <Select>
                    { tiposContato.map( tipoContato => adicionarOption( tipoContato ) ) }
                  </Select>
                </Form.Item>

                <IconMinusCircleOutlined clicar={ () => remove( name ) } />
              </Space>
            ) ) }
            <Form.Item>
              <Button type="dashed" onClick={ () => add() } block icon={ <IconPlusCircleOutlined /> }>
                Adicionar contato extra
              </Button>
            </Form.Item>
          </>
        ) }
      </Form.List>

      <Button className="submit" type="primary" htmlType="submit" >Cadastrar</Button>
    </Form>
  );
}

export default ClienteForm;
