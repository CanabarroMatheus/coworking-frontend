import { Layout, Menu } from 'antd';
import { Link } from 'react-router-dom';
import { converterStringParaIcone } from '../../helpers/mapIcons';
import { IconHomeOutlined } from './Icons';
import GenericIcon from '../generic-icon/GenericIcon';

function AntSider( props ) {
  const { Sider } = Layout;
  const { tema, modo, itensSelecionados, itens, colapsavel, colapsado, colapsar } = props;

  const addItem = ( item ) => {
    return (
      <Menu.Item key={ `item${ item.ORDEM }` } icon={ <GenericIcon component={ item.ICONE } rotacao={ item.ROTACAO } /> } >
        <Link to={ item.LINK }>
          { item.TITULO }
        </Link>
      </Menu.Item>
    );
  }

  const subMenu = ( subMenuItem ) => {
    return (
      <Menu.SubMenu key={ `sub${ subMenuItem.ORDEM }` } icon={ <GenericIcon component={ subMenuItem.ICONE } /> } title={ subMenuItem.TITULO } >
        { subMenuItem.ITENS.map( ( item ) => addItem( converterStringParaIcone( item ) ) ) }
      </Menu.SubMenu>
    );
  }

  return (
    <Sider className="sidebar" collapsible={ colapsavel } collapsed={ colapsado } onCollapse={ colapsar } >
      <Menu theme={ tema } mode={ modo } defaultSelectedKeys={ itensSelecionados }>
        <Menu.Item key="item0" icon={ <GenericIcon component={ IconHomeOutlined } /> } >
          <Link to="/home">
            Home
          </Link>
        </Menu.Item>
        { itens.map( ( subMenuItem ) => subMenu( converterStringParaIcone( subMenuItem ) ) ) }
      </Menu>
    </Sider>
  );
}

export default AntSider;
