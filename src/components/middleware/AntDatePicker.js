import { DatePicker } from 'antd'

function AntDatePicker( props ) {
  const { nome, tamanho, desabilitado, selecionador } = props;
  return <DatePicker name={ nome } size={ tamanho } disabled={ desabilitado } picker={ selecionador } />
}

export default AntDatePicker;