import { Button } from 'antd';

function AntButton( props ) {
  const { classe, tipo, acao, clique, children } = props;
  return <Button className={ classe } type={ tipo } htmlType={ acao } onClick={ clique }>{ children }</Button>
}

export default AntButton;
