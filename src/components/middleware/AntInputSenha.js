import { Input } from 'antd';

function AntInputSenha( props ) {
  const { nome ,tamanho, placeholder, prefixo, sufixo, desabilitado, borda } = props;
  return <Input.Password name={ nome } size={ tamanho } placeholder={ placeholder } prefix={ prefixo } suffix={ sufixo } disabled={ desabilitado } bordered={ borda } />
}

export default AntInputSenha;