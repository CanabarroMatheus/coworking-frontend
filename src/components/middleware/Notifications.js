import { notification } from 'antd';

export const openSuccessNotification = ( { mensagem, descricao } ) => {
  notification['success']({
    message: mensagem,
    description: descricao
  });
}

export const openInfoNotification = ( { mensagem, descricao } ) => {
  notification['info']({
    message: mensagem,
    description: descricao
  });
}

export const openWarningNotification = ( { mensagem, descricao } ) => {
  notification['warning']({
    message: mensagem,
    description: descricao
  });
}

export const openErrorNotification = ( { mensagem, descricao } ) => {
  notification['error']({
    message: mensagem,
    description: descricao
  });
}