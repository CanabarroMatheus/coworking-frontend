import { Input } from 'antd';

function AntInput( props ) {
  const { nome, tamanho, placeholder, prefixo, sufixo, desabilitado, borda } = props;
  return <Input name={ nome } size={ tamanho } placeholder={ placeholder } prefix={ prefixo } suffix={ sufixo } disabled={ desabilitado } bordered={ borda } />
}

export default AntInput;