import { Form } from 'antd';
import AntButton from './AntButton';
// import GenericIcon from '../generic-icon/GenericIcon';
import inserirInput from '../../helpers/mapInputs';

import { converterStringParaIcone } from '../../helpers/mapIcons'

function AntForm( props ) {

  const { nome, classe, acao, itens, classeBotaoSubmit, botaoSubmitTexto } = props;

  const addItem = ( item, i ) => {
    return (
      <Form.Item key={ i } label={ item.LABEL } name={ item.NOME } rules={ [{ required: item.OBRIGATORIO, message: item.MENSAGEM_OBRIGATORIO }] } >
        { inserirInput( item ) }
      </Form.Item>
    );
  }

  return (
    <Form layout="horizontal" name={ nome } className={ classe } onFinish={ acao } >
      { itens && itens.map( ( input, i ) => addItem( converterStringParaIcone( input ), i ) ) }
      <AntButton classe={ classeBotaoSubmit } tipo="primary" acao="submit">{ botaoSubmitTexto }</AntButton>
    </Form>
  );
}

export default AntForm;
